import React, { Component } from 'react';
import logo from './logo.png';
import './App.scss';
import Search from './components/search/Search';
import Card from './components/card/Card';
import InactiveCard from './components/inactive-card/InactiveCard';
import _ from 'lodash';
import moment from 'moment';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {data: [], initialData: []};
  }

  componentDidMount() {

    fetch("https://boiling-mountain-49639.herokuapp.com/desafio-frontend")
    .then(res => res.json())
    .then(result => {
      this.setState({data: result, initialData: result});
    })
    
  }

  calculeWaiting(lines) {

    let waiting = 0;

    Object.keys(lines).forEach(key => {
      let value = lines[key];
      waiting += value.waiting;
    });

    return waiting;
  }

  secondsToHourFormat(seconds) {
    return moment().startOf('day')
    .seconds(seconds)
    .format('H:mm:ss')
  }

  calculeElapsedTime (lines) {

    let totalElapsed = 0;
    let totalLines = 0;

    Object.keys(lines).forEach(key => {
      let value = lines[key];
      totalElapsed += Math.round(value.elapsed)
      totalLines += 1;
    });

    return this.secondsToHourFormat(Math.round(totalElapsed / totalLines));
  }

  getCard (branchOffice) {

    const waiting = this.calculeWaiting(branchOffice.lines)
    const elapsedTime = this.calculeElapsedTime(branchOffice.lines);

    switch (branchOffice.online) {
      case true:
        return <Card key={branchOffice.name} name={branchOffice.name} waiting={waiting} elapsedTime={elapsedTime}/>
      case false:
        return <InactiveCard key={branchOffice.name} name={branchOffice.name}  waiting={waiting} elapsedTime={elapsedTime} />
    }

  }

  search(e){

    let filterData = _.filter(this.state.initialData, function(o) { 
      const name = o.name.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase();
      const value = e.target.value.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase();
      return  name.includes(value)
    });

    this.setState({data: filterData });
  }

  render() {

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </header>
        <div className="App-search" >
        <div className="App-search-content">
          <Search onChange={this.search.bind(this)}/>
        </div>
        </div>
        <div className="App-content">
          {this.state.data.map(branchOffice =>    this.getCard(branchOffice) ) }
        </div>
      </div>
    );
  }
}

export default App;
