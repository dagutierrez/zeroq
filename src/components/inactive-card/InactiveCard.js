import React, { Component } from 'react';
import './InactiveCard.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class InactiveCard extends Component {

  render() {
  
    return (
      <div className="Card-box">
      <div className="Card-container-1 Container-1-inactive">
        <p>{this.props.name}</p>
      </div>
      <div className="Card-container-2">
        <FontAwesomeIcon className="Card-icon-1 Card-footer" icon="user" />
        <p className="Card-quantity Card-footer">{this.props.waiting}</p>
    
        <FontAwesomeIcon className="Card-icon-2  Card-footer" icon="clock" />
        <p className="Card-clock Card-footer">{this.props.elapsedTime}</p>
      </div>
    </div>
    );
  }
}

export default InactiveCard;