import React, { Component } from 'react';
import './Search.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class Search extends Component {
  render() {
    return (
      <div className="Box">
        <div className="Container-1">
          <FontAwesomeIcon className="icon" icon="search" />
          <input type="search" id="search" placeholder="Buscar sucursal" onChange={this.props.onChange} />
        </div>
      </div>
    );
  }
}

export default Search;
