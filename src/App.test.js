import React from 'react';
import ReactDOM from 'react-dom';
import { create } from "react-test-renderer";
import App from './App';

import { library } from '@fortawesome/fontawesome-svg-core'
import { faSearch, faUser, faClock } from '@fortawesome/free-solid-svg-icons'

library.add(faSearch)
library.add(faUser)
library.add(faClock)

let instance;
const oneLineMock = {"servipag-lider-arica-atencion-general":{"waiting":12,"elapsed":174.70760233918128,"name":"Atención General"}};
const multipleLineMock = {"servipag-escuela-agricola-cheque-deposito":{"waiting":8,"name":"Cheque Depósito","elapsed":245.76666666666668},"servipag-escuela-agricola-atencion-general":{"waiting":21,"wait":197,"name":"Atención General","elapsed":341.53164556962014}};

beforeEach(() => {
  const component = create(<App  />);
  instance = component.getInstance();
})

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('calculeWaiting function should be return expected result when there is only one line' , () => {
  const expectResult = 12;
  expect(instance.calculeWaiting(oneLineMock)).toBe(expectResult);
});

it('calculeWaiting function should sum the waiting number when there is more than one line', () => {
  const expectResult = 29;
  expect(instance.calculeWaiting(multipleLineMock)).toBe(expectResult);
});

it('calculeElapsedTime function should be return expected result when there is only one line', () => {
  const expectResult = "0:02:55";
  expect(instance.calculeElapsedTime(oneLineMock)).toBe(expectResult);
});

it('calculeElapsedTime function should be return expected result when there is only one line', () => {
  const expectResult = "0:04:54";
  expect(instance.calculeElapsedTime(multipleLineMock)).toBe(expectResult);
});

